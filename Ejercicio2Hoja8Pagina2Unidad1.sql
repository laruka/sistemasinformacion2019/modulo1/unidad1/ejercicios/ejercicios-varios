﻿/*EJERICIO2 HOJA 8 UNIDAD 2 PAGINA2*/

DROP DATABASE IF EXISTS ejercicio2;
CREATE DATABASE ejercicio2;

USE ejercicio2;

CREATE OR REPLACE TABLE zona_urbana(
  nombreZona varchar(50),
  categoria varchar(50),
  PRIMARY KEY (nombreZona)
  );


CREATE OR REPLACE TABLE bloque_casas(
  calle varchar(50),
  numero int,
  Npisos int,
  NombreZona varchar(50),
  PRIMARY KEY (calle, numero),
  CONSTRAINT fkbloquezona FOREIGN KEY (NombreZona) REFERENCES zona_urbana (nombreZona) 
  );

CREATE OR REPLACE TABLE casa_particular(
  NombreZona varchar(20),
  NumeroCasa int,
  metros int,
  PRIMARY KEY (nombreZona, NumeroCasa),
  CONSTRAINT fkcasazona FOREIGN KEY (NombreZona) REFERENCES zona_urbana (nombreZona)
  );


CREATE OR REPLACE TABLE piso(
  calle varchar(20),
  numero int,
  planta varchar(20),
  puerta int,
  metros int,
  PRIMARY KEY (calle, numero, planta, puerta),
  CONSTRAINT fkpisobloque FOREIGN KEY (calle, numero) REFERENCES bloque_casas (calle, numero)
  );

CREATE OR REPLACE TABLE persona(
  dni int,
  nombre varchar(20),
  edad int,
  NombreZona varchar(20),
  NumeroCasa int,
  calle varchar(20),
  numero int,
  planta varchar(20),
  puerta int,
  PRIMARY KEY (dni),
  CONSTRAINT fkpersonapiso FOREIGN KEY (calle, numero, planta, puerta) REFERENCES piso (calle, numero, planta, puerta)
  );

CREATE OR REPLACE TABLE poseec(
  NombreZona varchar(20),
  NumeroCasa int,
  dni int NOT NULL,
  PRIMARY KEY (dni, NombreZona, NumeroCasa),
  CONSTRAINT fkposeeccasa FOREIGN KEY (NombreZona, NumeroCasa) REFERENCES casa_particular (NombreZona, NumeroCasa),
  CONSTRAINT fkposeepcasa FOREIGN KEY (dni) REFERENCES persona (dni)
  );

CREATE OR REPLACE TABLE poseep(
  calle varchar(20),
  numero int,
  planta varchar(20),
  puerta int,
  dni int NOT NULL,
  PRIMARY KEY (calle, numero, planta, puerta, dni),
  CONSTRAINT fkposeepiso FOREIGN KEY (calle, numero, planta, puerta) REFERENCES piso (calle, numero, planta, puerta),
  CONSTRAINT fkposeepcasa1 FOREIGN KEY (dni) REFERENCES persona (dni)
  );







