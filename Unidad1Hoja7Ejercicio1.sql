﻿/*HOJA 7 EJERCICIO 1*/

DROP DATABASE IF EXISTS HOJA7EJ1;
CREATE DATABASE HOJA7EJ1;

USE HOJA7EJ1;

CREATE OR REPLACE TABLE departamento(
  cod_dpo varchar(10),
  PRIMARY KEY (cod_dpo)
);

CREATE OR REPLACE TABLE pertenece(
  cod_dpto varchar(10),
  dni varchar(10),
  PRIMARY KEY (cod_dpto),
  UNIQUE KEY (dni)
);

CREATE OR REPLACE TABLE empleado(
  dni_empleado varchar(10),
  PRIMARY KEY (dni_empleado)
);

CREATE OR REPLACE TABLE proyecto(
  cod_proyecto varchar(10),
  PRIMARY KEY (cod_proyecto)
);


CREATE OR REPLACE TABLE trabaja(
  dni_t varchar(10),
  cod_proy varchar(10),
  fecha date,
  PRIMARY KEY (dni_t),
  CONSTRAINT fktrabajaempleado FOREIGN KEY (dni_t) REFERENCES empleado (dni_empleado),
  CONSTRAINT fktrabajaproyecto FOREIGN KEY (cod_proy) REFERENCES proyecto (cod_proy)
);

